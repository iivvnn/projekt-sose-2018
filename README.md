## Description

This project was developed in the summer semester 2018 for the practical project of the study subject Media Informatics (Bachelor) at the Beuth University Berlin.

The idea to create a sports map with freely accessible and free sports facilities came from the group. 

The goal was to implement the following use cases:

As a user I want to be able to see locations of sports facilities.
As a user I would like to be able to filter sports facilities.
As a user I want to be able to add sports facilities.

In addition, the following points were implemented out of my own interest:

Admin area to edit entries (all CRUD operations).
To make the app available as Progressive Web App.
To deploy a public instance on Heroku.

The following technologies were used:

- React (with the Create-React-App) 
- MongoDB hosted on mLab.com
- Cloudinary as CDN for the images
- Deployment on a Heroku Server
- Google's Material UI Components 
- Prettier for code formatting

The project was implemented agile with SCRUM methods. There were the following roles:

Ray K. - Frontend
Lena N. - Backend
Tadeo M. - Backend
Christian K. - Frontend
Hanna M. - Scrum Master, Frontend
Iven Gehrmann - Product Owner, Frontend


[Link to presentation](https://docs.google.com/presentation/d/1S8aQgaXvbKv99mhhbqmbJCTodbikGwX1fH10G6cYOy4/edit?usp=sharing)

## Yarn installieren

[Installation](https://yarnpkg.com/lang/en/docs/install)


## Run local instance

```
git clone https://bitbucket.org/iivvnn/projekt-sose-2018
cd projekt-sose-2018
yarn install
```

## Scripts

### Dev mode

```
yarn start
```

This script is very powerful: It starts the server, the watchmode for JS, JSX, CSS, SCSS files. Also Prettier is activated. This module automatically formats the code. Eslint runs in the background. Additionally we have the possibility to use Flow. Flow is a simple alternative to TypeScript. 

All warnings and errors are displayed via the terminal.

More info:

[Prettier - Opinionated Code Formatter](https://prettier.io/) <br>
[http://localhost:3000](http://localhost:3000) 


### Build production
App  bundled in Production Mode.

```
yarn run build
```


### Dev enviroment

Install the Eslint plugin for your editor. 
