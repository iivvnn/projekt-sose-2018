import React from 'react';
import { HashRouter, Route } from 'react-router-dom';
import AdminArea from '../App/Components/AdminArea/AdminArea';
import Dialog from '../App/Components/Dialog/Dialog';
import App from '../App/App';

const Router = () => (
  <HashRouter>
    <div>
      <Route exact={true} path="/" component={App} />
      <Route path="/admin" component={AdminArea} />
    </div>
  </HashRouter>
);

export default Router;
