import React from 'react';
import './App.css';
import Map from './Components/Map/Map';
import { FilterIcon } from './Components/FilterIcon/filterIcon';
import PingPong from './Icons/active_tabletennis.svg';
import Basketball from './Icons/active_basketball.svg';
import Football from './Icons/active_football.svg';
import Volleyball from './Icons/active_volleyball.svg';

import iPingPong from './Icons/inactive_tabletennis.svg';
import iBasketball from './Icons/inactive_basketball.svg';
import iFootball from './Icons/inactive_football.svg';
import iVolleyball from './Icons/inactive_volleyball.svg';

import axios from 'axios';

import SimpleDialogDemo from './Components/Dialog/Dialog';

class App extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      visibleMenu: false,
      visibleButtons: true,
      visibleButton: true,
      selectedPingPongButton: true,
      selectedBasketButton: true,
      selectedFootballButton: true,
      selectedVolleyButton: true,
      pins: '',
    };

    this.handleMouseDown = this.handleMouseDown.bind(this);
    this.toggleMenu = this.toggleMenu.bind(this);
    this.toggleButtons = this.toggleButtons.bind(this);
    this.toggleFilterIcon = this.toggleFilterIcon.bind(this);
  }

  handleMouseDown(e) {
    if (
      e.target.id === 'menuIcon' ||
      (e.target.className === 'closeWindow' && this.state.visibleMenu)
    ) {
      this.toggleButtons();
      this.toggleMenu();
    }
    e.stopPropagation();
  }

  toggleMenu() {
    this.setState({
      visibleMenu: !this.state.visibleMenu,
    });
  }

  toggleButtons() {
    this.setState({
      visibleButtons: !this.state.visibleButtons,
    });
  }

  toggleFilterIcon(e) {
    if (e.target.id === 'pingPong') {
      this.setState({
        selectedPingPongButton: !this.state.selectedPingPongButton,
      });
    }
    if (e.target.id === 'Basketball') {
      this.setState({
        selectedBasketButton: !this.state.selectedBasketButton,
      });
    }
    if (e.target.id === 'Volleyball') {
      this.setState({
        selectedVolleyButton: !this.state.selectedVolleyButton,
      });
    }
    if (e.target.id === 'Football') {
      this.setState({
        selectedFootballButton: !this.state.selectedFootballButton,
      });
    }
  }

  getPinsFromDB = () => {
    axios
      .get('https://usm-backend.herokuapp.com/geojson', {
        responseType: 'GeoJSON',
      })
      .then(res => {
        this.setState({ pins: res.data });
        console.log('UPDATE ');
      })
      .catch(e => {
        console.log(e);
      });
  };

  componentDidMount() {
    this.getPinsFromDB();
  }

  render() {
    return (
      <div className="App">
        <Map
          height="100vh"
          width="100vw"
          geocoder={true}
          geolocate={true}
          centeredMarker={false}
          filterState={this.state}
          marker={false}
          requiredFilter={true}
          pins={this.state.pins}
        />

        <div className="FilterIconContainer">
          <FilterIcon
            id="pingPong"
            src={this.state.selectedPingPongButton ? PingPong : iPingPong}
            alt="pingPong"
            toggleFilterIcon={this.toggleFilterIcon}
            handleSelected={this.state.selectedPingPongButton}
          />
          <FilterIcon
            id="Basketball"
            src={this.state.selectedBasketButton ? Basketball : iBasketball}
            alt={'basketball'}
            toggleFilterIcon={this.toggleFilterIcon}
            handleSelected={this.state.selectedBasketButton}
          />
          <FilterIcon
            id="Volleyball"
            src={this.state.selectedVolleyButton ? Volleyball : iVolleyball}
            alt={'volleyball'}
            toggleFilterIcon={this.toggleFilterIcon}
            handleSelected={this.state.selectedVolleyButton}
          />
          <FilterIcon
            id="Football"
            src={this.state.selectedFootballButton ? Football : iFootball}
            alt={'football'}
            toggleFilterIcon={this.toggleFilterIcon}
            handleSelected={this.state.selectedFootballButton}
          />
        </div>

        <SimpleDialogDemo
          className="menuItem"
          handleUpdate={this.getPinsFromDB}
        />
      </div>
    );
  }
}

export default App;
