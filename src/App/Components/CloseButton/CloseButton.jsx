import React, { Component } from 'react';
import windowClose from '../../Icons/window-close.svg';

class CloseButton extends Component {
  render() {
    return (
      <button>
        <img
          className="closeWindow"
          src={windowClose}
          alt="Close Icon"
          onMouseDown={this.props.handleMouseDown}
        />
      </button>
    );
  }
}

export default CloseButton;
