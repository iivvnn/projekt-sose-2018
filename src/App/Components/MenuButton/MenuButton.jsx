import React, { Component } from 'react';
import './MenuButton.css';
import menuIcon from '../../Icons/baseline-add-24px.svg';

class MenuButton extends Component {
  render() {
    let visibility = 'hide';

    if (this.props.handleButtonVisibility) {
      visibility = 'show';
    }

    return (
      <img
        className={'show'}
        id="menuIcon"
        src={menuIcon}
        alt="Menu Icon"
        onMouseDown={this.props.handleMouseDown}
      />
    );
  }
}

export default MenuButton;
