import { Component } from 'react';
import { Map } from 'mapbox-gl';
import PropTypes from 'prop-types';
import {} from 'dotenv/config';
import '@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css';
import * as MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';

class Geocoder extends Component {
  static contextTypes = { map: PropTypes.object.isRequired };

  context: {
    map: Map,
  };

  componentDidMount() {
    const { map } = this.context;

    map.addControl(
      new MapboxGeocoder({
        accessToken:
          'pk.eyJ1IjoiaXZlbmxvbmdlcngiLCJhIjoiY2lqbGNqZGY5MDAxd3V5bHhpbmJ1bHNqNyJ9.HPZv35F5mV6ssSSJtLuxWg',
      }),
    );
  }

  render() {
    return null;
  }
}

export default Geocoder;
