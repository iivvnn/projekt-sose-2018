import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactMapboxGl, {
  GeoJSONLayer,
  Layer,
  Marker,
  Popup,
} from 'react-mapbox-gl';
import './Map.css';

import CardMedia from '@material-ui/core/CardMedia';

import Geocoder from '../../Components/Geocoder/Geocoder';
import Mapbox, * as MapboxGL from 'mapbox-gl';
import axios from 'axios';
import AlertDialogSlide from '../AlertDialogSlide/AlertDialogSlide';
import { withStyles } from '@material-ui/core/styles/index';

const TOKEN =
  'pk.eyJ1IjoiaXZlbmxvbmdlcngiLCJhIjoiY2lqbGNqZGY5MDAxd3V5bHhpbmJ1bHNqNyJ9.HPZv35F5mV6ssSSJtLuxWg';
const url = 'https://usm-backend.herokuapp.com/geojson';
const styleID = 'mapbox://styles/ivenlongerx/cjh7s1vs11h022spbhmjxh8sd';

const BERLIN_COORDINATES = [13.404954, 52.520008];

const MapBoxMap = ReactMapboxGl({
  accessToken: TOKEN,
});

const styles = theme => ({
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
    margin: 0,
    marginBottom: 10,
    paddingLeft: 0,
    paddingRight: 0,
  },
});

let pingpong = 'pingpong';
let basketball = 'basketball';
let soccer = 'soccer';
let volleyball = 'volleyball';
let positionPopUp = null;
let description = '';
let place = '';
let image = '';
let id = '';
let timeStamp = '';
let street = '';
let number = '';
let city = '';

const symbolLayout: MapboxGL.SymbolLayout = {
  'text-line-height': 1,
  'text-padding': 0,
  'text-anchor': 'bottom',
  'text-allow-overlap': true,
  'text-field': String.fromCharCode(0xf041),
  'text-font': ['Font Awesome 5 Free Solid'],
  'icon-optional': true,
  'text-size': 19,
  visibility: 'visible',
};

const symbolPaint: MapboxGL.SymbolPaint = {
  'text-translate-anchor': 'viewport',
  'text-color': {
    type: 'categorical',
    property: 'place',
    stops: [
      ['pingpong', '#EC5276'],
      ['basketball', '#FF805B'],
      ['volleyball', '#3C979D'],
      ['soccer', '#4AC584'],
    ],
  },
};

const symbolPopup: MapboxGL.Popup = {
  closeOnClick: true,
  color: '#545454',
  fontWeight: 500,
  padding: 4,
  width: 300,
};

class Map extends Component {
  constructor(props) {
    super(props);

    this.state = {
      popUpVisible: false,
      pins: null,
      markerPosition: this.props.center,
      markerPositionSet: false,
    };

    this.onStyleLoad = this.onStyleLoad.bind(this);
    this.handleEvent = this.handleEvent.bind(this);
    this.filterGeoLayer = this.filterGeoLayer.bind(this);
    this.symbolOnClick = this.symbolOnClick.bind(this);
    this.closePopUp = this.closePopUp.bind(this);
  }

  filterGeoLayer() {
    if (this.props.filterState.selectedPingPongButton) {
      pingpong = 'pingpong';
    } else {
      pingpong = '';
    }

    if (this.props.filterState.selectedBasketButton) {
      basketball = 'basketball';
    } else {
      basketball = '';
    }
    if (this.props.filterState.selectedFootballButton) {
      soccer = 'soccer';
    } else {
      soccer = '';
    }
    if (this.props.filterState.selectedVolleyButton) {
      volleyball = 'volleyball';
    } else {
      volleyball = '';
    }
  }

  //
  // componentDidMount() {
  //   this.props.marker
  //     ? null
  //     : axios
  //       .get(url, { responseType: 'GeoJSON' })
  //       .then(res => {
  //         this.setState({ pins: res.data });
  //       })
  //       .catch(e => {
  //         console.log(e);
  //       });
  // }

  onStyleLoad = (map, e) => {
    map.addControl(
      new Mapbox.GeolocateControl({
        positionOptions: {
          enableHighAccuracy: true,
        },
        showUserLocation: true,
      }),
    );
  };

  sendCoordinatesToParent = () => {
    if (this.props.marker && this.state.markerPositionSet) {
      console.log('MAP SEND');
      this.props.getCoordinatesFromChild({
        markerPosition: this.state.markerPosition,
        center: this.state.markerPosition,
      });
      this.setState({ markerPositionSet: true });
    }
  };

  handleEvent = (map, e) => {
    if (this.props.marker) {
      this.setState({
        markerPosition: [e.lngLat.lng, e.lngLat.lat],
        center: [e.lngLat.lng, e.lngLat.lat],
        markerPositionSet: true,
        showAlert: false,
      });
    }
  };

  symbolOnClick(e) {
    positionPopUp = [
      e.features[0].geometry.coordinates[0],
      e.features[0].geometry.coordinates[1],
    ];

    description = e.features[0].properties.name;
    image = e.features[0].properties.image;
    id = e.features[0].properties.id;
    let date = e.features[0].properties.created_at;
    timeStamp = date.split('T')[0];
    console.log(timeStamp);

    try {
      let address = e.features[0].properties.address;
      street = address.split(',')[1].replace(/['"]+/g, '');
      street = street.split(':')[1];
      number = address.split(',')[2];
      number = number.split(':')[1];
      city = address.split(',')[4];
      city = city.split(':')[1].replace(/['"]+/g, '');

      console.log(e.features[0].properties.address.street);

      console.log(e.features[0].properties);
    } catch (e) {
      street = '';
      number = '';
      city = '';
      console.log('keine Adresse vorhanden');
    }

    this.setState({
      popUpVisible: !this.state.popUpVisible,
    });
  }

  sendReport(id) {
    let url = 'https://usm-backend.herokuapp.com/spots/' + id;
    let data = {
      spot: {
        reported: true,
      },
    };
    axios
      .put(url, data)
      .then(res => {
        console.log(JSON.stringify(res));
      })
      .then(this.setState({ showAlert: true }))

      .catch(e => {
        console.log(e);
      });
  }

  componentWillUnmount = () => {
    if (this.state.markerPositionSet) {
      this.sendCoordinatesToParent();
      console.log('MAP UPDATE');
    }
  };

  closePopUp() {
    this.setState({
      popUpVisible: false,
    });
  }

  render() {
    this.props.requiredFilter ? this.filterGeoLayer() : null;
    const layerOptions: MapboxGL.layerOptions = {
      filter: ['in', 'place', pingpong, basketball, soccer, volleyball],
    };

    const { classes } = this.props;

    return (
      <div>
        <MapBoxMap
          // eslint-disable-next-line no-use-before-define
          style={styleID}
          center={this.props.center}
          zoom={this.props.zoom}
          containerStyle={{
            height: this.props.height,
            width: this.props.width,
          }}
          onClick={this.props.marker ? this.handleEvent : this.closePopUp}
          onStyleLoad={this.props.geolocate ? this.onStyleLoad : null}
        >
          {this.props.marker ? (
            <Layer
              type="symbol"
              id="marker"
              layout={{
                'icon-image': 'baseline-clear-24px',
                'icon-size': 1.6,
              }}
            >
              <Marker anchor="bottom" coordinates={this.state.markerPosition} />
            </Layer>
          ) : (
            <GeoJSONLayer
              data={this.props.pins}
              symbolLayout={symbolLayout}
              layerOptions={layerOptions}
              symbolPaint={symbolPaint}
              symbolOnClick={this.symbolOnClick}
            />
          )}
          {this.state.popUpVisible ? (
            <Popup
              coordinates={positionPopUp}
              style={symbolPopup}
              offset={{
                'bottom-left': [12, -38],
                bottom: [0, -38],
                'bottom-right': [-12, -38],
              }}
            >
              <div id="outerDiv">
                <CardMedia className={classes.media} image={image} />
                <br />
                <div className="description">{description.split(':')[0]}</div>
                <div className="address">{street}</div>

                <div className="timestamp">added: {timeStamp}</div>
                <button
                  className="reportButton"
                  onClick={() => this.sendReport(id)}
                >
                  Report
                </button>
              </div>
            </Popup>
          ) : null}
          {this.props.geocoder ? <Geocoder /> : null}
        </MapBoxMap>

        {this.state.showAlert ? (
          <AlertDialogSlide name="Report">
            Thanks for your report!
          </AlertDialogSlide>
        ) : null}
      </div>
    );
  }
}
export default withStyles(styles)(Map);

Map.propTypes = {
  center: PropTypes.arrayOf(PropTypes.number),
  markerPosition: PropTypes.arrayOf(PropTypes.number),
  geocoder: PropTypes.bool.isRequired,
  geolocate: PropTypes.bool.isRequired,
  marker: PropTypes.bool.isRequired,
  zoom: PropTypes.PropTypes.arrayOf(PropTypes.number),
};

Map.defaultProps = {
  zoom: [11],
  center: BERLIN_COORDINATES,
};
