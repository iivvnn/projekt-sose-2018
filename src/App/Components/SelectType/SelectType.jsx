import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});

class SimpleSelect extends React.Component {
  state = {
    type: '',
  };

  handleChange = event => {
    this.setState({ type: event.target.value });
  };

  render() {
    const { classes } = this.props;

    return (
      <form className={classes.root} autoComplete="off">
        <FormControl className={classes.formControl}>
          <Select
            value={this.state.type}
            onChange={this.handleChange}
            input={<Input name="type" id="type-helper" />}
          >
            <MenuItem value={'Ping-Pong'}>Ping-Pong</MenuItem>
            <MenuItem value={'Soccer'}>Soccer</MenuItem>
            <MenuItem value={'Volleyball'}>Volleyball</MenuItem>
            <MenuItem value={'Basketball'}>Basketball</MenuItem>
          </Select>
          <FormHelperText>Select spot type</FormHelperText>
        </FormControl>
      </form>
    );
  }
}

SimpleSelect.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleSelect);
