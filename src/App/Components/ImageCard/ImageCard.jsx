import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import classNames from 'classnames';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import { componentHeight } from '../ProgressMobileStepper/ProgressMobileStepper';

const styles = theme => ({
  card: {
    width: '100%',
    zIndex: 100,
    margin: 0,
    padding: 0,
    borderRadius: 0,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
    margin: 0,
    paddingLeft: 0,
    paddingRight: 0,
  },
  content: {
    height: componentHeight / 1.8,
    margin: 'auto',
    verticalAlign: 'middle',
    width: '100%',
    padding: 0,
  },

  wrapper: {
    paddingTop: 65,
    position: 'absolute',
    textAlign: 'center',
    width: '100%',
  },
});

class ImageCard extends Component {
  render() {
    const { classes } = this.props;

    return (
      <div>
        <Card className={classNames(classes.card, this.props.styling)}>
          <CardMedia className={classes.media} image={this.props.image}/>
          <CardContent className={classes.content}>
            <div className={classes.wrapper}>
              {this.props.type || this.props.state ?

                <Typography
                  className={classes.content}
                  variant="headline"
                  component="h2"
                >
                  {this.props.type}
                  <br/>
                  {this.props.sub}
                </Typography>
                : null
              }
            </div>
          </CardContent>
        </Card>
      </div>
    );
  }
}

ImageCard.propTypes = {
  classes: PropTypes.object.isRequired,
  image: PropTypes.string.isRequired,
};

export default withStyles(styles)(ImageCard);
