/* eslint-disable react/no-multi-comp */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import ProgressMobileStepper from '../../Components/ProgressMobileStepper/ProgressMobileStepper';
import MenuButton from '../../Components/MenuButton/MenuButton';

const styles = {
  button: {
    paddingLeft: 0,
    position: 'relative',
  },

  root: {
    margin: 0,
  },
};

class SimpleDialog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      dialogOpen: false,
    };
  }

  handleClose = () => {
    this.props.onClose(this.props.selectedValue);
  };
  // eslint-disable-next-line no-use-before-define
  handleListItemClick = value => {
    this.props.onClose(value);
  };

  render() {
    const { classes, onClose, selectedValue, ...other } = this.props;

    return (
      <Dialog
        className={classes.root}
        aria-labelledby="simple-dialog-title"
        {...other}
      >
        {this.props.children}
      </Dialog>
    );
  }
}

SimpleDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,
  selectedValue: PropTypes.string,
};

const SimpleDialogWrapped = withStyles(styles)(SimpleDialog);

class SimpleDialogDemo extends React.Component {
  state = {
    open: false,
  };

  handleClickOpen = e => {
    console.log(e.target.id);
    this.setState({
      open: true,
    });
  };

  handleCloseOnClick = e => {
    if (e.target.className === 'MuiBackdrop-root-14') {
      this.setState({ open: false });
    }
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <div onClick={this.handleClickOpen}>
          <MenuButton />
        </div>
        <SimpleDialogWrapped
          selectedValue={this.state.selectedValue}
          open={this.state.open}
          onMouseUp={e => this.handleCloseOnClick(e)}
        >
          <ProgressMobileStepper
            closeDialog={this.handleClose}
            handleUpdate={this.props.handleUpdate}
            handleCloseOnClick={e => this.handleClickOpen(e)}
          />
        </SimpleDialogWrapped>
      </div>
    );
  }
}

export default withStyles(styles)(SimpleDialogDemo);
