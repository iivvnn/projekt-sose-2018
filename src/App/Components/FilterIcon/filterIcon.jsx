import React, { Component } from 'react';

export class FilterIcon extends Component {
  render() {
    return (
      <div className="FilterIcon">
        <button className="FilterItem" onClick={this.props.toggleFilterIcon}>
          <img id={this.props.id} src={this.props.src} alt={this.props.alt} />
        </button>
      </div>
    );
  }
}
