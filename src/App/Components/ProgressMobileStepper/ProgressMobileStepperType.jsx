import React, { Component, PureComponent } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';

import { componentHeight } from '../ProgressMobileStepper/ProgressMobileStepper';

const styles = theme => ({
  container: {
    boxSizing: 'border-box',
  },

  input: {
    display: 'none',
  },
  label: {
    padding: 0,
  },
  image: {
    maxWidth: '100vw',
  },
  mapWrapper: {
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  button: {
    margin: theme.spacing.unit,
    width: '100%',
  },
  isActive: {
    backgroundColor: '#d3d3d3',
  },
  isInactive: {
    backgroundColor: 'rgba(225,225,225,.5)',
  },

  bootstrapRoot: {
    color: 'black',
    borderRadius: 0,
    height: componentHeight / 4,

    boxShadow: 'none',
    textTransform: 'none',
    fontSize: 16,
    minWidth: '100%',
    padding: '8px 16px',
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),

    '&:hover': {
      backgroundColor: '#d3d3d3',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#d3d3d3',
    },

    bootstrapRootInactive: {
      backgroundColor: '#d3d3d3',
      color: 'black',
      borderRadius: 0,
      height: componentHeight / 4,

      boxShadow: 'none',
      textTransform: 'none',
      fontSize: 16,
      minWidth: '100%',
      padding: '8px 16px',
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
    },
  },
});

class ProgressMobileStepperType extends Component {
  constructor(props) {
    super(props);
  }

  setBasketBall = () => {
    this.props.callbackFromParent({ type: 'basketball' });
  };
  setVolleyBall = () => {
    this.props.callbackFromParent({ type: 'volleyball' });
  };

  setPingPong = () => {
    this.props.callbackFromParent({ type: 'pingpong' });
  };

  setSoccer = () => {
    this.props.callbackFromParent({ type: 'soccer' });
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.container}>
        <Button
          data-info="pingpong"
          fullWidth={true}
          id="pingpong"
          onClick={this.setPingPong}
          className={classNames(
            classes.bootstrapRoot,
            classes.margin,
            this.props.type === 'pingpong'
              ? classes.isActive
              : classes.isInactive,
          )}
        >
          Ping-Pong
        </Button>
        <Divider />
        <Button
          id="basketball"
          onClick={this.setBasketBall}
          className={classNames(
            classes.bootstrapRoot,
            classes.margin,
            this.props.type === 'basketball'
              ? classes.isActive
              : classes.isInactive,
          )}
        >
          Basketball
        </Button>

        <Divider />
        <Button
          id="volleyball"
          variant="raised"
          onClick={this.setVolleyBall}
          className={classNames(
            classes.bootstrapRoot,
            classes.margin,
            this.props.type === 'volleyball'
              ? classes.isActive
              : classes.isInactive,
          )}
        >
          Volleyball
        </Button>

        <Divider />
        <Button
          id="soccer"
          onClick={this.setSoccer}
          className={classNames(
            classes.bootstrapRoot,
            classes.margin,
            this.props.type === 'soccer'
              ? classes.isActive
              : classes.isInactive,
          )}
        >
          Soccer
        </Button>
      </div>
    );
  }
}

export default withStyles(styles)(ProgressMobileStepperType);
