import React, { Component, PureComponent } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Map from '../../Components/Map/Map';
import {
  componentHeight,
  componentWidth,
} from '../ProgressMobileStepper/ProgressMobileStepper';
import { Layer, Marker } from 'react-mapbox-gl';
import PropTypes from 'prop-types';

const styles = theme => ({
  input: {
    display: 'none',
  },
  label: {
    padding: 0,
  },
  mapWrapper: {
    marginLeft: 'auto',
    marginRight: 'auto',
  },

  button: {},
});

class ProgressMobileStepperLocation extends PureComponent {
  constructor(props) {
    super(props);
  }

  getType = type => {
    if (type === 'pingpong') {
      return 'Ping-Pong';
    }
    if (type === 'volleyball') {
      return 'Volleyball';
    }
    if (type === 'soccer') {
      return 'Soccer';
    }
    if (type === 'basketball') {
      return 'Basketball';
    }
  };

  sendCoordinatesFromLocationToStepperParent = state => {
    if (state !== '') {
      console.log(state.markerPosition);
      this.convertCoordinatesToStreetname(state.markerPosition);
      this.props.callbackFromParent(state, {
        locationSet: true,
      });
    }
  };

  getFeature = json => {
    console.log('HIER');
    console.log(json);

    return json.features;
  };

  convertCoordinatesToStreetname = ([lng, lat]) =>
    fetch(
      'https://api.mapbox.com/geocoding/v5/mapbox.places/' +
        lng +
        ',' +
        lat +
        '.json?access_token=pk.eyJ1IjoiaXZlbmxvbmdlcngiLCJhIjoiY2lqbGNqZGY5MDAxd3V5bHhpbmJ1bHNqNyJ9.HPZv35F5mV6ssSSJtLuxWg',
    )
      .then(response => response.json())
      .then(mapboxJson => this.getFeature(mapboxJson))
      .then(feature =>
        this.props.callbackFromParent({
          streetname:
            feature[0].place_type[0] === 'address'
              ? feature[0].text
              : feature[0].place_type[0] === 'poi'
                ? feature[0].place_name.split(',')[1]
                : 'No address available',
          street:
            feature[0].place_type[0] === 'address'
              ? feature[0].text
              : feature[0].place_type[0] === 'poi'
                ? feature[0].place_name.split(',')[1]
                : 'No address available',

          postalcode: parseInt(
            feature[1].place_type[0] === 'locality'
              ? feature[1].place_name.split(',')[1]
              : '00000',
          ),
          city:
            feature[0].place_type[2] === 'region' || 'place'
              ? feature[2].place_name
              : 'No City info available',
        }),
      );

  error = err => console.warn(`ERROR(${err.code}): ${err.message}`);

  render() {
    const { classes } = this.props;
    return (
      <div id={this.props.id} className={classes.mapWrapper}>
        <Map
          id={this.props.id}
          height={componentHeight}
          width={componentWidth}
          geocoder={true}
          geolocate={true}
          marker={true}
          center={this.props.center}
          markerPosition={this.props.center}
          getCoordinatesFromChild={
            this.sendCoordinatesFromLocationToStepperParent
          }
        />
      </div>
    );
  }
}

export default withStyles(styles)(ProgressMobileStepperLocation);

ProgressMobileStepperLocation.propTypes = {
  markerPosition: PropTypes.arrayOf(PropTypes.number),
};

ProgressMobileStepperLocation.defaultProps = {
  zoom: [11],
};
