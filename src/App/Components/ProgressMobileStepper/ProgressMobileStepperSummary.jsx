import React, { PureComponent } from 'react';
import { withStyles } from '@material-ui/core/styles';
import ImageCard from '../ImageCard/ImageCard';
import { componentHeight } from '../ProgressMobileStepper/ProgressMobileStepper';
import Map from '../Map/Map';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import defaultImage from '../../Images/default.png';

import Typography from '@material-ui/core/Typography';
import withTheme from '@material-ui/core/styles/withTheme';

const styles = theme => ({
  paper: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3,
  }),

  root: {
    width: '100%',
    maxWidth: 500,
  },

  container: {
    boxSizing: 'border-box',
  },

  input: {
    display: 'none',
  },
  label: {
    padding: 0,
  },

  mapWrapper: {
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  button: {
    margin: theme.spacing.unit,
    width: '100%',
  },

  bootstrapRoot: {
    backgroundColor: 'rgba(225,225,225,.5)',
    color: 'black',
    borderRadius: 0,
    height: componentHeight / 2,
    boxShadow: 'none',
    textTransform: 'none',
    fontSize: 16,
    minWidth: '100%',
    padding: '8px 16px',
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      backgroundColor: '#d3d3d3',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#d3d3d3',
    },

    imageWrapper: {
      width: '100%',
      maxHeight: componentHeight / 2,
    },

    img: {
      height: componentHeight / 2,
      overflow: 'hidden',
      width: '100%',
    },

    name: {
      backgroundColor: 'red',
    },
  },
});

class ProgressMobileStepperSummary extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.container}>
        <div className={classes.name}>
          <ImageCard
            id="uploaded-image"
            image={this.props.image === '' ? defaultImage : this.props.image}
            type={this.props.type}
            sub={this.props.streetname}
          />
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(ProgressMobileStepperSummary);

ProgressMobileStepperSummary.propTypes = {
  /*
  image: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  center: PropTypes.arrayOf(PropTypes.number).isRequired,
  markerPosition: PropTypes.arrayOf(PropTypes.number).isRequired,
   */
};

ProgressMobileStepperSummary.defaultProps = {};
