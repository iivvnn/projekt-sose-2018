import React, { Component } from 'react';
import { ConfirmationBadge } from '../ConfirmationBadge/ConfirmationBadge';

class ProgressMobileStepperSuccess extends Component {
  render() {
    return (
      <div id={this.props.id}>
        <ConfirmationBadge />
      </div>
    );
  }
}

export default ProgressMobileStepperSuccess;
