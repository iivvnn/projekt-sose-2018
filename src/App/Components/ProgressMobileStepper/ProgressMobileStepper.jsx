import React, { Component, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import SwipeableViews from 'react-swipeable-views';

import ProgressMobileStepperSuccess from './ProgressMobileStepperSuccess';
import ProgressMobileStepperLocation from './ProgressMobileStepperLocation';
import ProgressMobileStepperUpload from './ProgressMobileStepperUpload';
import ProgressMobileStepperSummary from './ProgressMobileStepperSummary';
import ProgressMobileStepperType from './ProgressMobileStepperType';

import axios from 'axios';
import Map from '../Map/Map';

const rootHeight = 450;
const headerHeight = 45;
const stepperHeight = 50;

const BERLIN_COORDINATES = [13.404954, 52.520008];

export const componentHeight = rootHeight - headerHeight - stepperHeight;
export const componentWidth = 280;

const styles = theme => ({
  root: {
    width: '100%',
    height: rootHeight,
    flexGrow: 1,
    flexDirection: 'column',
    boxSizing: 'border-box',
    margin: 0,
  },

  rootroot: {
    width: componentWidth,
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    height: headerHeight,
    paddingLeft: theme.spacing.unit * 4,
    margin: 0,
    backgroundColor: theme.palette.background.default,
    width: '100vw' - theme.spacing.unit * 4,
    background: 'yellow',
  },

  mobileStepper: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    margin: 0,
    color: 'green',
    height: stepperHeight,
    boxSizing: 'border-box',
    zIndex: '100',
  },
  component: {
    height: componentHeight,
    width: '100%',
    overflow: 'hidden',
    margin: 0,
  },
});

class ProgressMobileStepper extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeStep: 0,
      image: '',
      imageBase64: '',
      lng: '',
      lat: '',
      type: '',
      dataSend: false,
      street: '',
      streetname: '',
      country: '',
      name: '',
      markerPosition: BERLIN_COORDINATES,
      center: BERLIN_COORDINATES,
    };
  }

  handleNext = () => {
    if (this.state.activeStep === 0 && this.state.type !== '') {
      this.setState(prevState => ({
        activeStep: prevState.activeStep + 1,
      }));
    }

    if (this.state.activeStep === 1 && this.state.markerPosition !== []) {
      this.setState(prevState => ({
        activeStep: prevState.activeStep + 1,
      }));
    }

    if (this.state.activeStep === 2) {
      this.setState(prevState => ({
        activeStep: prevState.activeStep + 1,
      }));
    }

    if (this.state.activeStep === 3) {
      this.setState(prevState => ({
        activeStep: prevState.activeStep + 1,
      }));
      this.sendDataToDB();
    }
  };

  handleBack = () => {
    this.setState(prevState => ({
      activeStep: prevState.activeStep - 1,
    }));
  };

  randomKey = pre =>
    pre +
    Math.random()
      .toString(36)
      .substring(2, 15) +
    Math.random()
      .toString(36)
      .substring(2, 15);

  handleStepChange = activeStep => {
    this.setState({ activeStep });
  };

  callBackCoord = dataFromChild => {
    //this.setState(Object.assign(this.state, dataFromChild));
    // console.log(dataFromChild);
    //this.callBack(dataFromChild);
    //console.log('PARENT: NEW STATE OF PARENT' + this.state.markerPosition);
  };

  callBack = dataFromChild => {
    this.setState(Object.assign(this.state, dataFromChild));

    dataFromChild.type ? this.setState({ name: dataFromChild.type }) : null;
    dataFromChild.streetname
      ? this.setState({
          name: this.getType(this.state.type) + ': ' + dataFromChild.streetname,
        })
      : null;

    console.log(this.state);

    //
    // this.state.activeStep === '5' && !this.state.dataSend
    //   ? this.sendDataToDB()
    //   : null;
    // console.log('⬇⬇⬇ PARENT: NEW STATE ⬇⬇⬇ ');
    // console.log(this.state);

    //this.setState({ markerPosition: dataFromChild });
    //console.log('PARENT: NEW STATE OF PARENT' + this.state.markerPosition);
  };

  later = (delay, value) =>
    new Promise(resolve => setTimeout(resolve, delay, value));

  sendDataToDB = () => {
    //    console.log('SEND 1');
    const data = {
      spot: {
        type: this.state.type,
        geometry: {
          coordinates: this.state.markerPosition,
        },
        address: {
          street: this.state.street,
          number: 123,
          postalcode: this.state.postalcode,
          city: this.state.city,
          state: this.state.city,
          country: this.state.city,
        },

        name: this.state.name,
      },
      image: this.state.imageBase64,
    };

    axios
      .post('https://usm-backend.herokuapp.com/spots', data)
      .then(res => {
        console.log(data);
        console.log(res);
        console.log(res.data);
      })
      .then(this.setState({ dataSend: true }))
      .then(this.later(2000))
      .then(this.props.handleUpdate)
      .then(this.props.closeDialog)
      .catch(error => {
        console.log(error.response);
        alert(error);
      });

    /*
    */
  };

  getType = type => {
    if (type === 'pingpong') {
      return 'Ping-Pong';
    }
    if (type === 'volleyball') {
      return 'Volleyball';
    }
    if (type === 'soccer') {
      return 'Soccer';
    }
    if (type === 'basketball') {
      return 'Basketball';
    }
  };

  render() {
    const { classes, theme } = this.props;
    const { activeStep } = this.state;

    //this.convertCoordinatesToStreetname(this.state.markerPosition);

    const addingSteps = [
      {
        id: '1',
        label: 'Please choose the type of your spot',
        component: (
          <ProgressMobileStepperType
            id={this.id}
            callbackFromParent={this.callBack}
            type={this.state.type}
          />
        ),
      },

      {
        id: '2',
        label: 'Set the coordinates',
        component: (
          <ProgressMobileStepperLocation
            id={this.id}
            center={this.state.center}
            callbackFromParent={this.callBack}
          />
        ),
      },

      {
        id: '3',

        label: 'Upload a picture of your spot',
        component: (
          <ProgressMobileStepperUpload
            id={this.id}
            image={this.state.image}
            callbackFromParent={this.callBack}
            title={'Please click on your picture'}
            type={this.state.type}
          />
        ),
      },

      {
        id: '4',
        label: 'Everything fine?',
        component: (
          <ProgressMobileStepperSummary
            id={this.id}
            name={this.state.name}
            streetname={this.state.streetname}
            type={this.getType(this.state.type)}
            image={this.state.image}
          />
        ),
      },
      {
        id: '5',
        label: 'Thank you!',
        component: (
          <ProgressMobileStepperSuccess
            id={this.id}
            callbackFromParent={this.callBack}
          />
        ),
      },
    ];
    const maxSteps = addingSteps.length;

    //markerPosition={this.state.markerPosition}
    //center={this.state.markerPosition}

    return (
      <div className={classes.rootroot}>
        <div className={classes.root}>
          <Paper square elevation={0} className={classes.header}>
            <Typography>{addingSteps[activeStep].label}</Typography>
          </Paper>
          <SwipeableViews
            className={classes.view}
            axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
            index={this.state.activeStep}
            onChangeIndex={this.handleStepChange}
            position="top"
            disabled
          >
            {addingSteps.map(step => (
              <div
                key={this.randomKey('addingSteps')}
                className={classes.component}
              >
                {step.component}
              </div>
            ))}
          </SwipeableViews>
          <MobileStepper
            variant="progress"
            steps={maxSteps}
            position="bottom"
            activeStep={activeStep}
            className={classes.mobileStepper}
            nextButton={
              <Button
                size="small"
                onClick={this.handleNext}
                disabled={activeStep === maxSteps - 1}
                id="button-next"
              >
                OK
                {theme.direction === 'rtl' ? (
                  <KeyboardArrowLeft />
                ) : (
                  <KeyboardArrowRight />
                )}
              </Button>
            }
            backButton={
              <Button
                size="small"
                onClick={this.handleBack}
                disabled={activeStep === 0}
                id="button-back"
              >
                {theme.direction === 'rtl' ? (
                  <KeyboardArrowRight />
                ) : (
                  <KeyboardArrowLeft />
                )}
                Back
              </Button>
            }
          />
        </div>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(ProgressMobileStepper);

ProgressMobileStepper.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

ProgressMobileStepper.defaultProps = {
  zoom: [11],
};
