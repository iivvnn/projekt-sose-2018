import React, { Component, PureComponent } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import defaultImage from '../../Images/default.png';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import ImageCard from '../ImageCard/ImageCard';
import { componentHeight } from '../ProgressMobileStepper/ProgressMobileStepper';
import AlertDialogSlide from '../AlertDialogSlide/AlertDialogSlide';
import base64Img from 'base-64';
import PropTypes from 'prop-types';

const MAX_IMAGE_SIZE = 6 * 1000000;

const styles = theme => ({
  paper: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3,
  }),

  container: {
    boxSizing: 'border-box',
  },

  input: {
    display: 'none',
  },
  label: {
    padding: 0,
  },

  mapWrapper: {
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  button: {
    margin: theme.spacing.unit,
    width: '100%',
  },

  bootstrapRoot: {
    backgroundColor: 'rgba(225,225,225,.5)',
    color: 'black',
    borderRadius: 0,
    height: componentHeight / 2,
    boxShadow: 'none',
    textTransform: 'none',
    fontSize: 16,
    minWidth: '100%',
    padding: '8px 16px',
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      backgroundColor: '#d3d3d3',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#d3d3d3',
    },

    imageWrapper: {
      width: '100%',
      maxHeight: componentHeight / 2,
    },

    img: {
      height: componentHeight / 2,
      overflow: 'hidden',
      width: '100%',
    },

    fullHeight: {
      height: [[componentHeight], '!important'],
    },
  },
});

class ProgressMobileStepperUpload extends PureComponent {
  constructor(props) {
    super(props);

    this.fileSelectHandler = this.fileSelectHandler.bind(this);

    this.state = {
      showAlert: false,
    };
  }

  fileSelectHandler = e => {
    const file = e.target.files[0];
    if (this.fileSizeIsOk(file) === true) {
      this.handleImage(file);
    } else this.setState({ showAlert: true });
  };

  fileSizeIsOk = file => {
    let value = null;
    file.size <= MAX_IMAGE_SIZE ? (value = true) : (value = false);
    return value;
  };

  toDataURL = (url, callback) => {
    let xhr = new XMLHttpRequest();
    xhr.onload = function() {
      let reader = new FileReader();
      reader.onloadend = function() {
        callback(reader.result);
      };
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
  };

  handleImage = file => {
    this.toDataURL(URL.createObjectURL(file), dataUrl =>
      this.props.callbackFromParent({
        image: URL.createObjectURL(file),
        imageBase64: dataUrl,
        imageSet: true,
      }),
    );
  };

  handleImageDefault = image => {
    this.toDataURL(image, dataUrl =>
      this.props.callbackFromParent({
        image: image,
        imageBase64: dataUrl,
        imageSet: true,
      }),
    );
  };

  setDefaultImage = () => {
    this.handleImageDefault(defaultImage);
  };

  setType = type => {
    if (type === 'pingpong') {
      return 'Ping-Pong';
    }

    if (type === 'volleyball') {
      return 'Volleyball';
    }

    if (type === 'soccer') {
      return 'Soccer';
    }

    if (type === 'basketball') {
      return 'Basketball';
    } else return 'Hey you, please set a spot type.';
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.container}>
        <input
          accept="image/*"
          className={classes.input}
          id="raised-button-file"
          multiple
          type="file"
          onChange={this.fileSelectHandler}
        />
        <label htmlFor="raised-button-file">
          {this.props.image === '' ? (
            <div>
              <Button
                variant="raised"
                component="span"
                className={classNames(classes.margin, classes.bootstrapRoot)}
              >
                Add a picture
              </Button>
              <Divider />

              <Button
                variant="contained"
                color="primary"
                disableRipple
                onClick={e => this.setDefaultImage(e)}
                className={classNames(classes.margin, classes.bootstrapRoot)}
              >
                Nah, don't have a picture.
              </Button>
            </div>
          ) : (
            <div>
              <ImageCard
                id="uploaded-image"
                image={this.props.image}
                type={this.setType(this.props.type)}
              />
            </div>
          )}
        </label>

        {this.state.showAlert ? (
          <AlertDialogSlide title="Warning!">
            Your image is too big. Please use an image with max. 6 MB.
          </AlertDialogSlide>
        ) : (
          ''
        )}
      </div>
    );
  }
}

export default withStyles(styles)(ProgressMobileStepperUpload);

ProgressMobileStepperUpload.propTypes = {
  image: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
};
