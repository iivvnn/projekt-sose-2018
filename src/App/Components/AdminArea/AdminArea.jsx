import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import RefreshIcon from '@material-ui/icons/Refresh';
import FilterListIcon from '@material-ui/icons/FilterList';
import { lighten } from '@material-ui/core/styles/colorManipulator';
import Modal from '@material-ui/core/Modal';

import React from 'react';
import './AdminArea.css';

let counter = 0; // Anzahl an Elementen in der Tabelle

let selectionArr = []; // Liste der ausgewählten Elemente
let allData = []; // Alle Daten

let modalImage = '';

const modalStyle = {
  marginTop: '5%',
};
const divStyle = {
  display: 'block',
  marginLeft: 'auto',
  marginRight: 'auto',
};

// Datenstruktur für die Tabelle
const columnData = [
  { id: 'type', numeric: false, disablePadding: true, label: 'type' },
  { id: 'reported', numeric: false, disablePadding: false, label: 'reported' },
  { id: 'name', numeric: false, disablePadding: false, label: 'name' },
  { id: 'note', numeric: false, disablePadding: false, label: 'note' },
  { id: 'image', numeric: false, disablePadding: false, label: 'image' },
  { id: 'longitude', numeric: true, disablePadding: false, label: 'longitude' },
  { id: 'latitude', numeric: true, disablePadding: false, label: 'latitude' },
  { id: 'extId', numeric: false, disablePadding: false, label: 'id' },
];

class EnhancedTableHead extends React.Component {
  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };

  render() {
    const {
      onSelectAllClick,
      order,
      orderBy,
      numSelected,
      rowCount,
    } = this.props;

    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === rowCount}
              onChange={onSelectAllClick}
            />
          </TableCell>
          {columnData.map(column => {
            return (
              <TableCell
                key={column.id}
                numeric={column.numeric}
                padding={column.disablePadding ? 'none' : 'default'}
                sortDirection={orderBy === column.id ? order : false}
              >
                <Tooltip
                  title="Sort"
                  placement={column.numeric ? 'bottom-end' : 'bottom-start'}
                  enterDelay={300}
                >
                  <TableSortLabel
                    active={orderBy === column.id}
                    direction={order}
                    onClick={this.createSortHandler(column.id)}
                  >
                    {column.label}
                  </TableSortLabel>
                </Tooltip>
              </TableCell>
            );
          }, this)}
        </TableRow>
      </TableHead>
    );
  }
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const toolbarStyles = theme => ({
  root: {
    paddingRight: theme.spacing.unit,
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  spacer: {
    flex: '1 1 10%',
  },
  actions: {
    color: theme.palette.text.secondary,
  },
  name: {
    flex: '0 0 auto',
  },
});

let EnhancedTableToolbar = props => {
  const { numSelected, classes } = props;
  return (
    <Toolbar
      className={classNames(classes.root, {
        [classes.highlight]: numSelected > 0,
      })}
    >
      <div className={classes.name}>
        {numSelected > 0 ? (
          <Typography color="inherit" variant="subheading">
            {numSelected} selected
          </Typography>
        ) : (
          <Typography variant="title" id="tableTitle">
            Spots
          </Typography>
        )}
      </div>
      <div className={classes.spacer} />
      <div className={classes.actions}>
        <Tooltip title="Refresh">
          <IconButton
            aria-label="Refresh"
            onClick={() => {
              // console.log('refresh pressed');
              window.location.reload();
            }}
          >
            <RefreshIcon />
          </IconButton>
        </Tooltip>

        <Tooltip title="Delete">
          <IconButton
            aria-label="Delete"
            onClick={() => {
              //    console.log(props);
              //     console.log(this.state);

              //das erste ausgewählt wird nicht richtig erkannt
              for (let i of selectionArr) {
                fetch(
                  'https://usm-backend.herokuapp.com/spots' +
                    '/' +
                    allData[i].extId,
                  { method: 'delete' },
                ).then(response => response.json());
              }

              if (window.confirm('Reload page now?')) {
                window.location.reload();
              }

              //alert("Reload window please");
              //window.location.reload();
            }}
          >
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      </div>
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
};

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  table: {
    minWidth: 1020,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
});

this.state = {};
let tableData = [];
class EnhancedTable extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      order: 'desc',
      orderBy: 'extId',
      selected: [],
      data: [],
      //data: tableData.sort((a, b) => (a.timestamp < b.timestamp ? -1 : 1)),
      page: 0,
      rowsPerPage: 5,
      open: false,
    };
  }
  componentDidMount() {
    fetch('https://usm-backend.herokuapp.com/spots')
      .then(response => response.json())
      .then(data => {
        // console.log('fetch get');
        for (let i = 0; i < data.length; i++) {
          tableData[i] = {
            id: i,
            type: data[i].type,
            reported: data[i].reported,
            name: data[i].name,
            note: data[i].note,
            image: data[i].image,
            longitude: data[i].geometry.coordinates[0],
            latitude: data[i].geometry.coordinates[1],
            extId: data[i]._id,
            intId: i,
          };

          if (data[i].type === undefined) {
            tableData[i].type = '-';
          }
          if (data[i].reported === undefined) {
            tableData[i].reported = '-';
          } else if (data[i].reported === true) {
            tableData[i].reported = 'true';
          } else {
            tableData[i].reported = 'false';
          }
          if (data[i].name === undefined) {
            tableData[i].name = '-';
          }
          if (data[i].note === undefined) {
            tableData[i].note = '-';
          }
          if (data[i].image === undefined) {
            tableData[i].image = '-';
          }
        }
        allData = JSON.parse(JSON.stringify(tableData));
        counter = data.length;

        this.setState({ data: tableData });
      });
  }

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    const data =
      order === 'desc'
        ? this.state.data.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
        : this.state.data.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

    this.setState({ data, order, orderBy });
  };

  handleSelectAllClick = (event, checked) => {
    if (checked) {
      this.setState({ selected: this.state.data.map(n => n.id) });
      return;
    }
    this.setState({ selected: [] });
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }
    this.setState({ selected: newSelected });
    //console.log('state selected: ' + this.state.selected); //--------------------------------------
    //selectionArr = this.state.selected;
    selectionArr = newSelected;
    // console.log('new selected: ' + newSelected);
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  handleOpen = imgS => {
    console.log('imgS: ' + imgS);
    modalImage = imgS;
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;
    const { data, order, orderBy, selected, rowsPerPage, page } = this.state;
    const emptyRows =
      rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

    return (
      <Paper className={classes.root}>
        <EnhancedTableToolbar numSelected={selected.length} />
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle">
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={this.handleSelectAllClick}
              onRequestSort={this.handleRequestSort}
              rowCount={data.length}
            />
            <Modal
              aria-labelledby="simple-modal-title"
              aria-describedby="simple-modal-description"
              open={this.state.open}
              onClose={this.handleClose}
            >
              <div style={divStyle}>
                <img style={modalStyle} src={modalImage} />
              </div>
            </Modal>
            <TableBody>
              {data
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map(n => {
                  const isSelected = this.isSelected(n.id);
                  return (
                    <TableRow
                      hover
                      aria-checked={isSelected}
                      tabIndex={-1}
                      key={n.id}
                      selected={isSelected}
                    >
                      <TableCell
                        padding="checkbox"
                        onClick={event => this.handleClick(event, n.id)}
                        role="checkbox"
                      >
                        <Checkbox checked={isSelected} />
                      </TableCell>
                      <TableCell component="th" scope="row" padding="none">
                        {n.type}
                      </TableCell>
                      <TableCell>{n.reported}</TableCell>
                      <TableCell>{n.name}</TableCell>
                      <TableCell>{n.note}</TableCell>
                      <TableCell onClick={() => this.handleOpen(n.image)}>
                        {n.image}
                      </TableCell>
                      <TableCell numeric>{n.longitude}</TableCell>
                      <TableCell numeric>{n.latitude}</TableCell>
                      <TableCell>{n.extId}</TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 49 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        <TablePagination
          component="div"
          count={data.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
      </Paper>
    );
  }
}

EnhancedTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EnhancedTable);
