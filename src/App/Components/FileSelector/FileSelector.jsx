import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import defaultImage from '../../Images/default.png';

const styles = theme => ({
  input: {
    display: 'none',
  },
  label: {
    padding: 0,
  },
  image: {
    maxWidth: '100vw',
  },
  mapWrapper: {
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  button: {
    margin: theme.spacing.unit,
  },
});

class FileSelector extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: this.props.defaultImage,
    };

    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'file' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  render() {
    const { classes } = this.props;

    return (
      <label htmlFor="raised-button-file">
        <input
          accept="image/*"
          className={classes.input}
          id="raised-button-file"
          multiple
          type="file"
          onChange={e => this.handleChange(e.target.files)}
        />

        <div className="button-container">
          <Button variant="raised" component="span" className={classes.Button}>
            Upload
          </Button>
        </div>
      </label>
    );
  }
}

export default withStyles(styles)(FileSelector);

FileSelector.propTypes = {
  defaultImage: PropTypes.string.isRequired,
};

FileSelector.defaultProps = {
  defaultImage: defaultImage,
};
