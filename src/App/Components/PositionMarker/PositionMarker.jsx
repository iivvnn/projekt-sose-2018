import React, { Component } from 'react';
import { Marker } from 'react-mapbox-gl';
import PropTypes from 'prop-types';
import clearIcon from '../../Icons/baseline-clear-24px.svg';

class MapMarker extends Component {
  render() {
    return (
      <Marker coordinates={this.props.center} anchor="bottom">
        <img src={this.props.icon} />
      </Marker>
    );
  }
}

export default MapMarker;

MapMarker.propTypes = {
  icon: PropTypes.string.isRequired,
  center: PropTypes.arrayOf(PropTypes.number),
};
