import React, { Component } from 'react';
import './ConfirmationBadge.css';

export class ConfirmationBadge extends Component {
  render() {
    return (
      <div>
        <br />
        <br />
        <br />
        <svg
          className="icon"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 60 60"
        >
          <circle className="circle" cx="30" cy="30" r="30" fill="none" />
          <path className="check" fill="none" d="m12.5 28l10.0 13 24-22.2" />
        </svg>
      </div>
    );
  }
}
