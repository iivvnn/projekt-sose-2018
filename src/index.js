import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import Router from './Router/Router';

ReactDOM.render(
  <div>
    <Router />
  </div>,
  document.getElementById('root'),
);
registerServiceWorker();
