module.exports = {
  'env': {
    'browser': true,
    'jest': true,
    'es6': true,
    'node': true,
  },
  'extends': [
    'airbnb',
    'prettier',
  ],
  'plugins': [
    'prettier',
    'flowtype-errors'

  ],
  'rules': {
    'prettier/prettier': ['error', {
      'singleQuote': true,
      'trailingComma': 'es5'
    }],
    "flowtype-errors/show-errors": 2,
    "flowtype-errors/show-warnings": 1,
    "flowtype-errors/enforce-min-coverage": [2, 50],
    "linebreak-style": 0

  },
  'parserOptions': {
    'ecmaFeatures': {
      'jsx': true,
    }
  }
}