/*const {rewireWorkboxInject, defaultInjectConfig} = require('react-app-rewire-workbox');
const path = require('path');

module.exports = function override(config, env) {
  if (env === "production") {
    console.log("Production build - Adding Workbox for PWAs");
    // Extend the default injection config with required swSrc
    const workboxConfig = {
      ...defaultInjectConfig,
      swSrc: path.join(__dirname, 'src', 'service-worker.js')
    };
    config = rewireWorkboxInject(workboxConfig)(config, env);
  }

  return config;
};
*/

/* config-overrides.js */
const { rewireWorkboxGenerate } = require('react-app-rewire-workbox');

module.exports = function override(config, env) {
  if (env === 'production') {
    console.log('Production build - Adding Workbox for PWAs');
    const workboxConfig = {
      skipWaiting: true,
      clientsClaim: true,
      runtimeCaching: [
        {
          urlPattern: new RegExp(
            '^https://fonts.(?:googleapis|gstatic).com/(.*)',
          ),
          handler: 'cacheFirst',
        },
        {
          urlPattern: /.*/,
          handler: 'networkFirst',
        },
      ],
      //exclude: "index.html"
    };
    config = rewireWorkboxGenerate(workboxConfig)(config, env);
    return config;
  }
};
